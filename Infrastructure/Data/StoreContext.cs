using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Enities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {

        }
        public DbSet<Product> Products {get;set;}
    }
}