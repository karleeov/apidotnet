using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Enities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}